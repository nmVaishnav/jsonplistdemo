//
//  TableViewController.m
//  blogReaderJson
//
//  Created by Naman on 8/3/16.
//  Copyright © 2016 RGAP. All rights reserved.
//

#import "TableViewController.h"
#import "blogPostClass.h"
//#import "WebViewController.h"
//#import "ImageViewController.h"
#import "SDWebImage/UIImageView+WebCache.h"
#import "MBProgressHUD.h"


NSComparisonResult sortByA(blogPostClass *firstBlog, blogPostClass *secondBlog){
    return [firstBlog.title compare:secondBlog.title];
}
NSComparisonResult sortByB(blogPostClass *firstBlog, blogPostClass *secondBlog){
    return [secondBlog.title compare:firstBlog.title];
}
NSString *path;
MBProgressHUD *hud;
@interface TableViewController ()
@end

@implementation TableViewController
@synthesize blogPosts;
@synthesize blogDataPosts;
@synthesize title;
@synthesize filteredBlogPosts;
@synthesize searchBar;
@synthesize sortedBlogPosts;
@synthesize segumentControl;
//@synthesize storeData;
@synthesize dictionary;


-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
//    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
//    hud.mode = MBProgressHUDModeIndeterminate;
//    hud.label.text = @"Uploading";
//    [hud showAnimated:YES];
    
}
-(void)viewWillDisappear:(BOOL)animated{
    
//    [hud hideAnimated:YES];
}
-(void)awakeFromNib{
    
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = MBProgressHUDModeIndeterminate;
    hud.label.text = @"Uploading";
    [hud showAnimated:YES];

}
- (void)viewDidLoad {
    [super viewDidLoad];
   
    NSError *error;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES); //1
    NSString *documentsDirectory = [paths objectAtIndex:0]; //2
    path = [documentsDirectory stringByAppendingPathComponent:@"blogs.plist"]; //3
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    if (![fileManager fileExistsAtPath: path]) //4
    {
        NSString *bundle = [[NSBundle mainBundle] pathForResource:@"blogs" ofType:@"plist"]; //5
        
        [fileManager copyItemAtPath:bundle toPath: path error:&error]; //6
    }
    
//    storeData = [[NSMutableArray alloc] initWithContentsOfFile:path];
    
    #pragma mark :: Read Data into pList . . .
    
    
    NSData *archiveData = [NSData dataWithContentsOfFile:path];
    id yourClassInstance = [NSKeyedUnarchiver unarchiveObjectWithData:archiveData];
//    NSLog(@"%@",yourClassInstance);
    blogPosts = [[NSMutableArray alloc]init];
    
//    [blogPosts removeAllObjects];
    
    int i =0;
    
    if ([yourClassInstance isKindOfClass:[NSArray class]]) {
        NSArray *dataFrompList = [[NSArray alloc]init];
        dataFrompList = yourClassInstance;
        for (i=0; i<dataFrompList.count; i++) {
            blogPostClass *blog = [[blogPostClass alloc]init];
            blog.title = [dataFrompList[i] valueForKey:@"title"];
            blog.author = [dataFrompList[i] valueForKey:@"author"];
            blog.thumbnil = [dataFrompList[i] valueForKey:@"thumbnil"];
            [blogPosts addObject:blog];
        }
        [self.tableView reloadData];
    }

    
    
    
    if (blogPosts.count == 0) {
        NSURL *blogURL = [NSURL URLWithString:@"http://blog.teamtreehouse.com/api/get_recent_summary/"];
        NSData *jsonData = [NSData dataWithContentsOfURL:blogURL];
        
        //    NSError *error = nil;
        NSDictionary *dataDictionary = [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:&error];
        
        //    blogPosts = [dataDictionary objectForKey:@"posts" ];
        
        
        
        blogPosts = [NSMutableArray array]; //Null object array
        
        NSArray *blogsArray = [dataDictionary objectForKey:@"posts"];
        
        for (NSDictionary *bpDictionary in blogsArray) {
            blogPostClass *blogPost = [[blogPostClass alloc] init];
            
            blogPost.title = [bpDictionary objectForKey:@"title"];
            blogPost.author = [bpDictionary objectForKey:@"author"];
            blogPost.thumbnil = [bpDictionary objectForKey:@"thumbnail"];
            blogPost.date = [bpDictionary objectForKey:@"date"];
//            blogPost.url = [NSURL URLWithString:[bpDictionary objectForKey:@"url"]];
            
            [self.blogPosts addObject:blogPost];
        }
        
        int i=0;
        //    for (i=0; i<blogPosts.count; i++) {
        //        dictionary = [[NSMutableDictionary alloc]init];
        //
        //        dictionary = [[blogPosts objectAtIndex:i] valueForKey:@"title"] ;
        ////        dictionary = [[blogPosts objectAtIndex:i] valueForKey:@"author"];
        //
        //        [storeData addObject:dictionary];
        //    }
        
        blogDataPosts = [[NSMutableArray alloc]init];
        BOOL flag = false;
        for (i=0; i<blogPosts.count; i++) {
            blogPostClass  *obj = [blogPosts objectAtIndex:i];
            NSMutableDictionary *blogDict = [[NSMutableDictionary alloc] init];
            [blogDict setObject:obj.title forKey:@"title"];
            [blogDict setObject:obj.author forKey:@"author"];
            [blogDict setObject:obj.thumbnil forKey:@"thumbnil"];
            
            
            [blogDataPosts addObject:blogDict];
            
            //            NSError  *error;
            
            
        }
        
#pragma mark :: Write Data into pList . . .
        NSData* archiveData = [NSKeyedArchiver archivedDataWithRootObject:blogDataPosts];
        //    NSData* archiveData = [NSKeyedArchiver archivedDataWithRootObject:blogPosts];
        flag =[archiveData writeToFile:path options:NSDataWritingAtomic error:&error];
        
        if (flag) {
            NSLog(@"Data Written");
        }
        else{
            NSLog(@"Data Not Written");
        }

    }else{
        
    }
    
    searchBar.delegate = self;
    
    
    
}


#pragma mark SearchBar Processing

-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    isFiltered = NO;
    [self.tableView reloadData];
}

-(void) searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    [self.tableView resignFirstResponder];
}

-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    if (searchText.length == 0) {
        isFiltered = NO;
    }else{
        isFiltered = YES;
        filteredBlogPosts = [[NSMutableArray alloc]init];
        for (blogPostClass *blog in blogPosts) {
            NSRange dicsRange = [blog.title rangeOfString:searchText options:NSCaseInsensitiveSearch];
            
            if (dicsRange.location != NSNotFound) {
                [filteredBlogPosts addObject:blog];
            }
        }
    }
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (isFiltered) {
         return filteredBlogPosts.count;
    }else{
        return blogPosts.count;
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CellId" forIndexPath:indexPath];
    
    
    if (isFiltered) {
        blogPostClass *blogs = [filteredBlogPosts objectAtIndex:indexPath.row];
      
        [cell.imageView sd_setImageWithURL:[NSURL URLWithString:blogs.thumbnil] placeholderImage:[UIImage imageNamed:@"1a.jpg"]];

        
        cell.textLabel.text = blogs.title;
        cell.detailTextLabel.text = blogs.author;
    }else{
        blogPostClass *blogs = [blogPosts objectAtIndex:indexPath.row];
        
        
        [cell.imageView sd_setImageWithURL:[NSURL URLWithString:blogs.thumbnil] placeholderImage:[UIImage imageNamed:@"1a.jpg"]];
        cell.textLabel.text = blogs.title;
        cell.detailTextLabel.text = blogs.author;
    }
  
 
    return cell;
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath: (NSIndexPath *)indexPath {

    //  NSLog(@"%d",indexPath.row);
    
//***************************Show Blogs into Browser *******************************************
//    blogPostClass *blogs = [blogPosts objectAtIndex:indexPath.row];
//    // Now we have to access our own application object(which is sigleton) and the it is declaired in main.m file . . . AKA  - creation of shared instance of our application object . . .
//    UIApplication *application = [UIApplication sharedApplication];
//    [application openURL:blogs.url]; // use this application object to open a url into default browser .  .

//***************************Show Blogs into Browser ******************************************
}
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
//    NSLog(@"Segue ID %@", segue.identifier);
//    if ([segue.identifier isEqualToString:@"showBlogPost"] ) {
//        
//        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
//        blogPostClass *blogs = [self.blogPosts objectAtIndex:indexPath.row];
//        
////        [segue.destinationViewController setBlogPostURL: blogs.url];
////----------------OR-----------------
//        
//        WebViewController *wbc = (WebViewController *)segue.destinationViewController;
//        wbc.blogPostURL =  blogs.url;
//        wbc.imageViewURL =blogs.thumbnil;
    
        
//        ImageViewController *imageViewController = [segue destinationViewController];
//        imageViewController.tableViewController = self;
        
        
    
        
//    }

}

//-(NSDictionary *)getImageData{
//  
//    return blogPosts[self.tableView.indexPathForSelectedRow.row];
//
//}


#pragma mark :: Segument Changed . . .

- (IBAction)SegmentChanged:(id)sender {
    
    if ([sender selectedSegmentIndex] == 0) {
        
        [blogPosts sortUsingFunction:sortByA context:nil];
        
//        blogPosts = [NSMutableArray arrayWithArray:[blogPosts sortedArrayUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
//            NSString *first = [(blogPostClass*)obj1 title];
//            NSString *second = [(blogPostClass*)obj2 title];
//            return [first compare:second];
//            //        return NSOrderedSame;
//        }]];
    }else if ([sender selectedSegmentIndex] == 1){
        [hud hideAnimated:YES];
        [blogPosts sortUsingFunction:sortByB context:nil];
        
//        blogPosts = [NSMutableArray arrayWithArray:[blogPosts sortedArrayUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
//            NSString *first = [(blogPostClass*)obj1 title];
//            NSString *second = [(blogPostClass*)obj2 title];
//            return [second compare:first];
//        }]];
    }
    
    [self.tableView reloadData];
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

#pragma mark :: Delete Record From pList

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [blogPosts removeObjectAtIndex:indexPath.row];
        
        blogDataPosts = [[NSMutableArray alloc]init];
        BOOL flag = false;
        int i =0;
        for (i=0; i<blogPosts.count; i++) {
            blogPostClass  *obj = [blogPosts objectAtIndex:i];
            NSMutableDictionary *blogDict = [[NSMutableDictionary alloc] init];
            [blogDict setObject:obj.title forKey:@"title"];
            [blogDict setObject:obj.author forKey:@"author"];
            
            [blogDataPosts addObject:blogDict];
            
            //            NSError  *error;
            
            
        }
        
        
        NSData* archiveData = [NSKeyedArchiver archivedDataWithRootObject:blogDataPosts];
        //    NSData* archiveData = [NSKeyedArchiver archivedDataWithRootObject:blogPosts];
        NSError *error;
        flag =[archiveData writeToFile:path options:NSDataWritingAtomic error:&error];

        if (flag) {
            NSLog(@"Data Deleted SuccessFully");
        }else{
            NSLog(@"Data isn't Deleted");
        }
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
