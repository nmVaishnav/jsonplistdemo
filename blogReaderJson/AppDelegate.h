//
//  AppDelegate.h
//  blogReaderJson
//
//  Created by Naman on 8/3/16.
//  Copyright © 2016 RGAP. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

