//
//  TableViewController.h
//  blogReaderJson
//
//  Created by Naman on 8/3/16.
//  Copyright © 2016 RGAP. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TableViewController : UITableViewController<UISearchBarDelegate>
{
    BOOL isFiltered;
}
@property (atomic,strong) NSMutableArray* blogPosts;
@property (atomic,strong) NSMutableArray* blogDataPosts;
@property (atomic,strong) NSArray* sortedBlogPosts;
@property (atomic,strong) NSMutableArray* filteredBlogPosts;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segumentControl;

//@property (strong,nonatomic) NSMutableArray *storeData;
@property(strong,nonatomic) NSMutableDictionary *dictionary;

//-(NSDictionary *)getImageData;
- (IBAction)SegmentChanged:(id)sender;


@end
